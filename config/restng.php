<?php

return [
    'restngUrl' => env('RESTNG_URL', 'https://wsdev.apps.miamioh.edu'),
    'sslVerify' => env('RESTNG_SSL_VERIFY', true),
    'credentials' => [
        'token' => [
            'default' => [
                'type' => 'usernamePassword',
                'username' => env('RESTNG_USERNAME', ''),
                'password' => env('RESTNG_PASSWORD', ''),
            ]
        ]
    ]
];