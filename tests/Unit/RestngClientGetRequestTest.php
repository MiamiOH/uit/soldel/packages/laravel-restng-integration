<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;

use MiamiOH\LaravelRestng\Auth\Token\UsernamePassword;
use MiamiOH\LaravelRestng\Exceptions\RestngException;
use MiamiOH\LaravelRestng\Responses\RestngPageableResponse;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelRestng\Utils\RestfulHttpClient;
use MiamiOH\LaravelRestng\Utils\RestfulHttpResponse;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RestngClientGetRequestTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $restfulHttpClient;
    /**
     * @var RestngClient
     */
    private $restngClient;
    /**
     * @var string
     */
    private $restngUrl;

    protected function setUp(): void
    {
        $this->restngUrl = 'https://fake-ws-host/api';
        $this->restfulHttpClient = $this->createMock(RestfulHttpClient::class);
        $this->restngClient = new RestngClient($this->restfulHttpClient, $this->restngUrl);
    }

    public function testGetRestngUrl()
    {
        $this->assertSame($this->restngUrl, $this->restngClient->getRestngUrl());
    }

    public function testMakeGetRequest()
    {
        $this->restfulHttpClient
            ->method('request')
            ->with(
                $this->equalTo('GET'),
                $this->equalTo('https://fake-ws-host/api/test/v1'),
                $this->equalTo(['attr1' => 3]),
                $this->equalTo([])
            )
            ->willReturn(new RestfulHttpResponse([
                'data' => ['something' => 'ok'],
                'status' => 200,
                'error' => false,
            ], 200));

        $response = $this->restngClient->get('/test/v1', ['attr1' => 3]);

        $this->assertEquals(['something' => 'ok'], $response->getData());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFalse($response->isPageable());
    }

    public function testUrlWithoutLeadingSlash()
    {
        $this->restfulHttpClient
            ->method('request')
            ->with(
                $this->equalTo('GET'),
                $this->equalTo('https://fake-ws-host/api/test/v1'),
                $this->equalTo(['attr1' => 3]),
                $this->equalTo([])
            )
            ->willReturn(new RestfulHttpResponse([
                'data' => ['something' => 'ok'],
                'status' => 200,
                'error' => false,
            ], 200));

        $response = $this->restngClient->get('test/v1', ['attr1' => 3]);

        $this->assertEquals(['something' => 'ok'], $response->getData());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFalse($response->isPageable());
    }

    public function testMakeRequestWithToken()
    {
        $tokenCredential = $this->createMock(UsernamePassword::class);
        $tokenCredential->method('getToken')->willReturn('token123');

        $this->restfulHttpClient
            ->method('request')
            ->with(
                $this->equalTo('GET'),
                $this->equalTo('https://fake-ws-host/api/test/v1'),
                $this->equalTo(['attr1' => 3, 'token' => 'token123']),
                $this->equalTo([])
            )
            ->willReturn(new RestfulHttpResponse([
                'data' => ['something' => 'ok'],
                'status' => 200,
                'error' => false,
            ], 200));

        $response = $this->restngClient->get('/test/v1', ['attr1' => 3], $tokenCredential);

        $this->assertEquals(['something' => 'ok'], $response->getData());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFalse($response->isPageable());
    }

    public function testMakeBadRequest()
    {
        $this->restfulHttpClient
            ->method('request')
            ->with(
                $this->equalTo('GET'),
                $this->equalTo('https://fake-ws-host/api/test/v1'),
                $this->equalTo(['attr1' => 'invalid-value']),
                $this->equalTo([])
            )
            ->willReturn(new RestfulHttpResponse([
                'data' => ['message' => 'something is wrong'],
                'status' => 400,
                'error' => true,
            ], 400));

        try {
            $this->restngClient->get('/test/v1', ['attr1' => 'invalid-value']);
            $this->fail();
        } catch (RestngException $e) {
            $this->assertEquals(['message' => 'something is wrong'], $e->getData());
            $this->assertEquals(400, $e->getStatusCode());
        }
    }

    public function testGetPageableResponse()
    {
        $this->restfulHttpClient
            ->method('request')
            ->with(
                $this->equalTo('GET'),
                $this->equalTo('https://fake-ws-host/api/test/v1'),
                $this->equalTo(['attr1' => 3]),
                $this->equalTo([])
            )
            ->willReturn(new RestfulHttpResponse([
                'data' => [
                    ['name' => 'Bob'],
                    ['name' => 'Jack'],
                    ['name' => 'Rob'],
                ],
                'status' => 200,
                'error' => false,
                'total' => '5',
                'currentUrl' => 'https://fake-ws-host/api/test/v1?limit=3&offset=1',
                'firstUrl' => 'https://fake-ws-host/api/test/v1?limit=3&offset=1',
                'lastUrl' => 'https://fake-ws-host/api/test/v1?limit=3&offset=3',
            ], 200));

        /** @var RestngPageableResponse $response */
        $response = $this->restngClient->get('/test/v1', ['attr1' => 3]);

        $this->assertTrue($response->isPageable());
        $this->assertInstanceOf(RestngPageableResponse::class, $response);
        $this->assertEquals([
            ['name' => 'Bob'],
            ['name' => 'Jack'],
            ['name' => 'Rob'],
        ], $response->getData());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(5, $response->getTotal());
        $this->assertEquals('https://fake-ws-host/api/test/v1?limit=3&offset=1', $response->getCurrentUrl());
        $this->assertEquals('https://fake-ws-host/api/test/v1?limit=3&offset=1', $response->getFirstUrl());
        $this->assertEquals('https://fake-ws-host/api/test/v1?limit=3&offset=3', $response->getLastUrl());
    }
}
