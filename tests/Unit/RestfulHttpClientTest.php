<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/3/18
 * Time: 7:37 PM
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;


use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use MiamiOH\LaravelRestng\Utils\Configuration;
use MiamiOH\LaravelRestng\Utils\RestfulHttpClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RestfulHttpClientTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $config;

    protected function setUp(): void {
        $this->config = $this->createMock(Configuration::class);
    }

    public function testGetResponse()
    {
        $mock = new MockHandler([
            new Response(200, [], '{"data":{"attr1":"abcd","attr2":3},"status":200,"error":false}')
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $this->config->method('isSslVerify')->willReturn(true);
        $restfulHttpClient = new RestfulHttpClient($client,  $this->config);
        $response = $restfulHttpClient->request('GET', '/test', [], []);

        $this->assertEquals([
            'data' => [
                'attr1' => 'abcd',
                'attr2' => 3
            ],
            'status' => 200,
            'error' => false
        ], $response->getResponseBody());

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCallToANonExistRoute()
    {
        $mock = new MockHandler([
            new Response(404, [], '')
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $this->config->method('isSslVerify')->willReturn(true);
        $restfulHttpClient = new RestfulHttpClient($client,  $this->config);
        $response = $restfulHttpClient->request('GET', '/test', [], []);

        $this->assertEquals([], $response->getResponseBody());

        $this->assertEquals(404, $response->getStatusCode());
    }
}
