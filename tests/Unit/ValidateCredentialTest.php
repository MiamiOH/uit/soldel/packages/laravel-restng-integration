<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 3/31/20
 * Time: 11:26 AM
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;


use Carbon\Carbon;
use MiamiOH\LaravelRestng\Auth\Token\TokenString;
use MiamiOH\LaravelRestng\Auth\Token\UsernamePassword;
use MiamiOH\LaravelRestng\Exceptions\RestngException;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\RestngClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ValidateCredentialTest extends TestCase
{
    /**
     * @var UsernamePassword
     */
    private $upCredential;
    /**
     * @var TokenString
     */
    private $tokenCredential;
    /**
     * @var MockObject
     */
    private $restngClient;

    protected function setUp(): void
    {
        $this->restngClient = $this->createMock(RestngClient::class);
        $this->upCredential = new UsernamePassword('user', 'pwd');
        $this->tokenCredential = new TokenString('liaom', 'alsdkjlasdf');
    }

    public function testUsernamePasswordIsCorrect()
    {
        $expiredAt = Carbon::now()->addHours(1);
        $this->restngClient
            ->expects($this->once())
            ->method('post')
            ->willReturn(new RestngResponse(
                [
                    'token' => 'token123',
                    'authenticationLocation' => 'https://fake-ws-host/api/authentication/v1/token123',
                    'username' => 'user',
                    'credentialSource' => 'LDAP',
                    'tokenLifetime' => $expiredAt->format('Y-m-d\TH:i:s'),
                ],
                200
            ));

        $this->assertTrue($this->upCredential->isValid($this->restngClient));
    }

    public function testUsernamePasswordIsNoValid()
    {
        $this->restngClient
            ->expects($this->once())
            ->method('post')
            ->willThrowException(new RestngException('POST', '/auth', [], 401));

        $this->assertFalse($this->upCredential->isValid($this->restngClient));
    }

    public function testTokenIsValid()
    {
        $expiredAt = Carbon::now()->addHours(1);
        $this->restngClient
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('/authentication/v1/alsdkjlasdf'))
            ->willReturn(new RestngResponse(
                [
                    'token' => 'alsdkjlasdf',
                    'username' => 'liaom',
                    'credential_source' => 'aaaa',
                    'expiration_time' => $expiredAt->format('Y-m-d\TH:i:s'),
                ],
                200
            ));

        $this->assertTrue($this->tokenCredential->isValid($this->restngClient));
    }

    public function testTokenIsNotValidIfTokenIsIncorrect()
    {
        $this->restngClient
            ->expects($this->once())
            ->method('get')
            ->willThrowException(new RestngException('GET', '/auth/aaa', [], 404));

        $this->assertFalse($this->tokenCredential->isValid($this->restngClient));
    }

    public function testTokenIsInvalidIfUsernameDoesNotMatch()
    {
        $expiredAt = Carbon::now()->addHours(1);
        $this->restngClient
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('/authentication/v1/alsdkjlasdf'))
            ->willReturn(new RestngResponse(
                [
                    'token' => 'alsdkjlasdf',
                    'username' => 'aaaaa',
                    'credential_source' => 'aaaa',
                    'expiration_time' => $expiredAt->format('Y-m-d\TH:i:s'),
                ],
                200
            ));

        $this->assertFalse($this->tokenCredential->isValid($this->restngClient));
    }

    public function testTokenIsInvalidIfAlreadyExpired()
    {
        $expiredAt = Carbon::now()->subHours(1);
        $this->restngClient
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('/authentication/v1/alsdkjlasdf'))
            ->willReturn(new RestngResponse(
                [
                    'token' => 'alsdkjlasdf',
                    'username' => 'liaom',
                    'credential_source' => 'aaaa',
                    'expiration_time' => $expiredAt->format('Y-m-d\TH:i:s'),
                ],
                200
            ));

        $this->assertFalse($this->tokenCredential->isValid($this->restngClient));
    }
}