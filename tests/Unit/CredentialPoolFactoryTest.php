<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;

use MiamiOH\LaravelRestng\Auth\Token\UsernamePassword;
use MiamiOH\LaravelRestng\Auth\Token\TokenString;
use MiamiOH\LaravelRestng\Exceptions\LaravelRestngException;
use MiamiOH\LaravelRestng\Laravel\CredentialPoolFactory;
use PHPUnit\Framework\TestCase;

class CredentialPoolFactoryTest extends TestCase
{
    /**
     * @var CredentialPoolFactory
     */
    private $poolFactory;

    protected function setUp(): void
    {
        $this->poolFactory = new CredentialPoolFactory();
    }

    public function testInvalidAuthType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'something' => [

            ]
        ]);
    }

    public function testInvalidCredentialConfigStructure()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => 'token'
            ]
        ]);
    }

    public function testMissingCredentialType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => [
                    'username' => 'aaa',
                    'password' => 'bbb'
                ]
            ]
        ]);
    }

    public function testInvalidCredentialType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => [
                    'type' => 'fake',
                    'username' => 'aaa',
                    'password' => 'bbb'
                ]
            ]
        ]);
    }

    public function testMissingUsernameInTheUsernamePasswordType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => [
                    'type' => 'usernamePassword',
                    'password' => 'bbb'
                ]
            ]
        ]);
    }

    public function testMissingPasswordInTheUsernamePasswordType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => [
                    'type' => 'usernamePassword',
                    'username' => 'bbb'
                ]
            ]
        ]);
    }

    public function testMissingTokenInTheTokenStringType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => [
                    'type' => 'tokenString',
                    'username' => 'test1'
                ]
            ]
        ]);
    }

    public function testMissingUsernameInTheTokenStringType()
    {
        $this->expectException(LaravelRestngException::class);
        $this->poolFactory->createFromConfigArray([
            'token' => [
                'default' => [
                    'type' => 'tokenString',
                    'token' => 'token123'
                ]
            ]
        ]);
    }

    public function testAddOneUsernamePasswordTypeCredential()
    {
        $pool = $this->poolFactory->createFromConfigArray([
            'token' => [
                'name2' => [
                    'type' => 'usernamePassword',
                    'username' => 'abc',
                    'password' => '123'
                ]
            ]
        ]);

        $this->assertInstanceOf(UsernamePassword::class, $pool->get('name2'));
        $this->assertEquals('abc', $pool->get('name2')->getUsername());
    }

    public function testAddOneTokenStringTypeCredential()
    {
        $pool = $this->poolFactory->createFromConfigArray([
            'token' => [
                'name1' => [
                    'type' => 'tokenString',
                    'username' => 'test1',
                    'token' => 'token123'
                ]
            ]
        ]);

        $this->assertInstanceOf(TokenString::class, $pool->get('name1'));
        $this->assertEquals('test1', $pool->get('name1')->getUsername());
    }
}
