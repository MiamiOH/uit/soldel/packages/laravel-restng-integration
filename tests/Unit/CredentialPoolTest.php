<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/4/18
 * Time: 12:21 AM
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;

use MiamiOH\LaravelRestng\Auth\Token\TokenString;
use MiamiOH\LaravelRestng\Exceptions\LaravelRestngException;
use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use PHPUnit\Framework\TestCase;

class CredentialPoolTest extends TestCase
{
    /**
     * @var CredentialPool
     */
    private $pool;

    protected function setUp(): void
    {
        $this->pool = new CredentialPool();
    }

    public function testAddCredential()
    {
        $credential1 = $this->mockTokenStringCredential('1234');
        $credential2 = $this->mockTokenStringCredential('2345');
        $this->pool->add($credential1, 'name1');
        $this->pool->add($credential2, 'name2');

        $this->assertEquals($this->getCredentials(), [
            'name1' => $credential1,
            'name2' => $credential2
        ]);
    }

    public function testRetrieveDefaultCredential()
    {
        $credential = $this->mockTokenStringCredential('1234');
        $this->pool->add($credential);
        $this->assertEquals($this->getCredentials()['default'], $this->pool->get());
    }

    public function testGetCredentialByName()
    {
        $credential = $this->mockTokenStringCredential('1234');
        $this->pool->add($credential, 'name');
        $this->assertEquals($this->getCredentials()['name'], $this->pool->get('name'));
    }

    public function testRemoveCredential()
    {
        $credential1 = $this->mockTokenStringCredential('1234');
        $credential2 = $this->mockTokenStringCredential('2345');
        $this->pool->add($credential1, 'name1');
        $this->pool->add($credential2, 'name2');
        $this->pool->remove('name2');

        $this->assertEquals($this->getCredentials(), [
            'name1' => $credential1
        ]);
    }

    public function testCredentialNotFound()
    {
        $this->expectException(LaravelRestngException::class);
        $this->pool->get('name');
    }

    private function mockTokenStringCredential(string $tokenString): TokenString
    {
        $mock = $this->createMock(TokenString::class);
        $mock->method('getToken')->willReturn($tokenString);
        return $mock;
    }

    private function getCredentials(): array
    {
        $reflection = new \ReflectionClass(CredentialPool::class);
        $map = $reflection->getProperty('map');
        $map->setAccessible(true);
        return $map->getValue($this->pool);
    }
}
