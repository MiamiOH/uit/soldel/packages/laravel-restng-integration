<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/3/18
 * Time: 8:52 PM
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;


use Carbon\Carbon;
use MiamiOH\LaravelRestng\Auth\Token\RestngToken;
use MiamiOH\LaravelRestng\Auth\Token\UsernamePassword;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\RestngClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetTokenByUsernamePasswordTest extends TestCase
{
    /**
     * @var UsernamePassword
     */
    private $credential;
    /**
     * @var MockObject
     */
    private $restngClient;

    protected function setUp(): void
    {
        $this->restngClient = $this->createMock(RestngClient::class);
        $this->credential = new UsernamePassword('user', 'pwd');
    }

    public function testGetToken()
    {
        $expiredAt = Carbon::now()->addHours(1);

        $this->restngClient
            ->method('post')
            ->with(
                $this->equalTo('/authentication/v1'),
                $this->equalTo([]),
                $this->equalTo([
                    'username' => 'user',
                    'password' => 'pwd',
                    'type' => 'usernamePassword'
                ])
            )
            ->willReturn(new RestngResponse(
                [
                    'token' => 'token123',
                    'authenticationLocation' => 'https://fake-ws-host/api/authentication/v1/token123',
                    'username' => 'user',
                    'credentialSource' => 'LDAP',
                    'tokenLifetime' => $expiredAt->format('Y-m-d\TH:i:s')
                ],
                200
            ));

        $token = $this->credential->getToken($this->restngClient);

        $reflection = new \ReflectionClass($this->credential);
        $reflactedRestngToken = $reflection->getProperty('restngToken');
        $reflactedRestngToken->setAccessible(true);

        /** @var RestngToken $restngToken */
        $restngToken = $reflactedRestngToken->getValue($this->credential);

        $this->assertEquals('token123', $token);
        $this->assertEquals('token123', $restngToken->getToken());
        $this->assertEquals(
            Carbon::createFromFormat(
                'Y-m-d\TH:i:s',
                $expiredAt->format('Y-m-d\TH:i:s')
            ),
            $restngToken->getExpiredAt());
        $this->assertEquals('LDAP', $restngToken->getSource());
        $this->assertEquals('user', $restngToken->getUsername());
        $this->assertFalse($restngToken->isExpired());
    }
}
