<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 11/28/18
 * Time: 1:49 PM
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;


use MiamiOH\LaravelRestng\Utils\Configuration;
use PHPUnit\Framework\TestCase;

class HttpClientConfigurationTest extends TestCase
{
    public function testDisableSslVerify() {
        $config = new Configuration(false);
        $this->assertFalse($config->isSslVerify());
    }
}