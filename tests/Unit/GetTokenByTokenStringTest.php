<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 11/28/18
 * Time: 1:53 PM
 */

namespace MiamiOH\LaravelRestng\Tests\Unit;


use MiamiOH\LaravelRestng\Auth\Token\TokenString;
use MiamiOH\LaravelRestng\RestngClient;
use PHPUnit\Framework\TestCase;

class GetTokenByTokenStringTest extends TestCase
{
    public function testGetUsername() {
        $auth = new TokenString('test1', 'abcd');
        $this->assertEquals('test1', $auth->getUsername());
    }

    public function testGetToken() {
        $auth = new TokenString('test1', 'abcd');
        $this->assertEquals('abcd', $auth->getToken($this->createMock(RestngClient::class)));
    }
}