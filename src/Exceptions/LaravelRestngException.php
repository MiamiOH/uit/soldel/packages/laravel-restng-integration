<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/3/18
 * Time: 9:34 PM
 */

namespace MiamiOH\LaravelRestng\Exceptions;

/**
 * Class LaravelRestngException
 * @package MiamiOH\LaravelRestng\Exceptions
 */
class LaravelRestngException extends \Exception
{
}
