<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Exceptions;

/**
 * Class RestngException
 * @package MiamiOH\LaravelRestng\Exceptions
 */

/**
 * Class RestngException
 * @package MiamiOH\LaravelRestng\Exceptions
 */
class RestngException extends \Exception
{
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $url;
    /**
     * @var array
     */
    private $data;
    /**
     * @var int
     */
    private $statusCode;

    /**
     * RestngException constructor.
     * @param string $method
     * @param string $url
     * @param array $data
     * @param int $statusCode
     */
    public function __construct(string $method, string $url, array $data, int $statusCode)
    {
        parent::__construct($data['message'] ?? sprintf(
            'RESTng responded status code "%s" with no error message (%s %s)',
            $statusCode,
            $method,
            $url
        ), $statusCode);
        $this->method = $method;
        $this->url = $url;
        $this->data = $data;
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
