<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Laravel;

use MiamiOH\LaravelRestng\Auth\Token\UsernamePassword;
use MiamiOH\LaravelRestng\Auth\Token\TokenString;
use MiamiOH\LaravelRestng\Exceptions\LaravelRestngException;

/**
 * Class CredentialPoolFactory
 * @package MiamiOH\LaravelRestng\Laravel
 */
class CredentialPoolFactory
{
    /**
     * @param array $credentials
     * @return CredentialPool
     * @throws LaravelRestngException
     */
    public function createFromConfigArray(array $credentials): CredentialPool
    {
        $pool = new CredentialPool();

        foreach ($credentials as $authType => $subCredentials) {
            if ($authType === 'token') {
                foreach ($subCredentials as $name => $subCredential) {
                    if (!is_array($subCredential)) {
                        throw new LaravelRestngException(sprintf('Invalid token credential (name: "%s", file: "config/restng.php")', $name));
                    }

                    if (!isset($subCredential['type'])) {
                        throw new LaravelRestngException(sprintf('Missing token credential type (name: "%s", file: "config/restng.php"). Available values are "usernamePassword" or "tokenString"', $name));
                    }

                    if (!in_array($subCredential['type'], ['usernamePassword', 'tokenString'])) {
                        throw new LaravelRestngException(sprintf('Invalid token credential type (name: "%s", file: "config/restng.php"). Available values are "usernamePassword" or "tokenString"', $name));
                    }

                    if ($subCredential['type'] === 'usernamePassword') {
                        if (!isset($subCredential['username']) || !isset($subCredential['password'])) {
                            throw new LaravelRestngException(sprintf('Missing "username" or "password" in the token credential (name: "%s", type: "usernamePassword", file: "config/restng.php"). ', $name));
                        }

                        $pool->add(new UsernamePassword($subCredential['username'], $subCredential['password']), $name);
                    }

                    if ($subCredential['type'] === 'tokenString') {
                        if (!isset($subCredential['username'])) {
                            throw new LaravelRestngException(sprintf('Missing "username" in the token credential (name: "%s", type: "tokenString", file: "config/restng.php"). ', $name));
                        }

                        if (!isset($subCredential['token'])) {
                            throw new LaravelRestngException(sprintf('Missing "token" in the token credential (name: "%s", type: "tokenString", file: "config/restng.php"). ', $name));
                        }

                        $pool->add(new TokenString(
                            $subCredential['username'],
                            $subCredential['token']
                        ), $name);
                    }
                }
            } else {
                throw new LaravelRestngException(sprintf('Invalid authentication type "%s"', $authType));
            }
        }

        return $pool;
    }
}
