<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/3/18
 * Time: 9:25 PM
 */

namespace MiamiOH\LaravelRestng\Laravel;

use MiamiOH\LaravelRestng\Auth\Authenticatable;
use MiamiOH\LaravelRestng\Exceptions\LaravelRestngException;

/**
 * Class CredentialPool
 * @package MiamiOH\LaravelRestng\Laravel
 */
class CredentialPool
{
    /**
     * @var array
     */
    private $map = [];

    /**
     * @param string $name
     * @return Authenticatable
     * @throws LaravelRestngException
     */
    public function get(string $name = 'default'): Authenticatable
    {
        if (!isset($this->map[$name])) {
            throw new LaravelRestngException(sprintf('Restng credential with name "%s" not found', $name));
        }

        return $this->map[$name];
    }

    /**
     * @param Authenticatable $auth
     * @param string $name
     */
    public function add(Authenticatable $auth, string $name = 'default'): void
    {
        $this->map[$name] = $auth;
    }

    /**
     * @param string $name
     */
    public function remove(string $name = 'default'): void
    {
        unset($this->map[$name]);
    }
}
