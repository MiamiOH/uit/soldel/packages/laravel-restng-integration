<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/3/18
 * Time: 9:15 PM
 */

namespace MiamiOH\LaravelRestng\Laravel\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\Laravel\CredentialPoolFactory;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelRestng\Utils\Configuration;
use MiamiOH\LaravelRestng\Utils\RestfulHttpClient;

/** @codeCoverageIgnore */
class LaravelRestngServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../../config/restng.php' => config_path('restng.php'),
        ], 'restng');

        $this->mergeConfigFrom(
            __DIR__.'/../../../config/restng.php',
            'restng'
        );
    }

    public function register()
    {
        $this->app->singleton(RestfulHttpClient::class, function () {
            $config = new Configuration(config('restng.sslVerify') ?? true);
            return new RestfulHttpClient(resolve(Client::class), $config);
        });

        $this->app->singleton(RestngClient::class, function () {
            return new RestngClient(resolve(RestfulHttpClient::class), config('restng.restngUrl'));
        });

        $this->app->singleton(CredentialPoolFactory::class, function () {
            return new CredentialPoolFactory();
        });

        $this->app->singleton(CredentialPool::class, function () {
            $credentials = config('restng.credentials') ?? [];

            /** @var CredentialPoolFactory $tokenCredentialPoolFactory */
            $tokenCredentialPoolFactory = resolve(CredentialPoolFactory::class);

            return $tokenCredentialPoolFactory->createFromConfigArray($credentials);
        });
    }

    public function provides()
    {
        return [
            RestfulHttpClient::class,
            RestngClient::class,
            CredentialPoolFactory::class,
            CredentialPool::class
        ];
    }
}
