<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 10/17/18
 * Time: 9:57 AM
 */

namespace MiamiOH\LaravelRestng\Utils;

class Configuration
{
    /**
     * @var bool
     */
    private $isSslVerify;

    /**
     * Configuration constructor.
     * @param bool $isSslVerify
     */
    public function __construct(bool $isSslVerify)
    {
        $this->isSslVerify = $isSslVerify;
    }

    /**
     * @return bool
     */
    public function isSslVerify(): bool
    {
        return $this->isSslVerify;
    }
}
