<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Utils;

/**
 * Class RestfulHttpResponse
 * @package MiamiOH\LaravelRestng\Utils
 */
class RestfulHttpResponse
{
    /**
     * @var array
     */
    private $responseBody;
    /**
     * @var int
     */
    private $statusCode;

    /**
     * RestfulHttpResponse constructor.
     * @param array $responseBody
     * @param int $statusCode
     */
    public function __construct(array $responseBody, int $statusCode)
    {
        $this->responseBody = $responseBody;
        $this->statusCode = $statusCode;
    }

    /**
     * @return array
     */
    public function getResponseBody(): array
    {
        return $this->responseBody;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
