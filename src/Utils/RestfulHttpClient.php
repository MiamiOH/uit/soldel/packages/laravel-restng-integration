<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Utils;

use GuzzleHttp\Client;

/**
 * Class RestfulHttpClient
 * @package MiamiOH\LaravelRestng\Utils
 */
class RestfulHttpClient
{
    /**
     * @var Client
     */
    private $httpClient;
    /**
     * @var Configuration
     */
    private $config;

    /**
     * RestfulHttpClient constructor.
     * @param Client $httpClient
     * @param Configuration $config
     */
    public function __construct(Client $httpClient, Configuration $config)
    {
        $this->httpClient = $httpClient;
        $this->config = $config;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @param array $body
     * @return RestfulHttpResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, string $url, array $options = [], array $body = []): RestfulHttpResponse
    {
        $response = $this->httpClient->request(
            $method,
            $url,
            [
                'query' => $options,
                'json' => $body,
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'http_errors' => false,
                'verify' => $this->config->isSslVerify()
            ]
        );

        $statusCode = $response->getStatusCode();
        $responseBody = json_decode($response->getBody()->getContents(), true) ?? [];

        return new RestfulHttpResponse(
            $responseBody,
            $statusCode
        );
    }
}
