<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng;

use MiamiOH\LaravelRestng\Auth\Authenticatable;
use MiamiOH\LaravelRestng\Exceptions\RestngException;
use MiamiOH\LaravelRestng\Responses\RestngPageableResponse;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\Responses\RestngResponseInterface;
use MiamiOH\LaravelRestng\Utils\RestfulHttpClient;

/**
 * Class RestngClient
 * @package MiamiOH\LaravelRestng
 */
class RestngClient
{
    /**
     * @var RestfulHttpClient
     */
    private $httpClient;
    /**
     * @var string
     */
    private $restngUrl;

    /**
     * @return string
     */
    public function getRestngUrl(): string
    {
        return $this->restngUrl;
    }

    /**
     * RestngClient constructor.
     * @param RestfulHttpClient $httpClient
     * @param string $restngUrl
     */
    public function __construct(RestfulHttpClient $httpClient, string $restngUrl)
    {
        $this->httpClient = $httpClient;
        $this->restngUrl = $restngUrl;
    }

    /**
     * @param string $url
     * @param array $options
     * @param Authenticatable|null $auth
     * @return RestngResponseInterface
     * @throws RestngException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $url, array $options = [], Authenticatable $auth = null): RestngResponseInterface
    {
        return $this->request('GET', $url, $options, [], $auth);
    }

    /**
     * @param string $url
     * @param array $options
     * @param array $body
     * @param Authenticatable|null $auth
     * @return RestngResponseInterface
     * @throws RestngException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $url, array $options = [], array $body = [], Authenticatable $auth = null): RestngResponseInterface
    {
        return $this->request('POST', $url, $options, $body, $auth);
    }

    /**
     * @param string $url
     * @param array $options
     * @param array $body
     * @param Authenticatable|null $auth
     * @return RestngResponseInterface
     * @throws RestngException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put(string $url, array $options = [], array $body = [], Authenticatable $auth = null): RestngResponseInterface
    {
        return $this->request('PUT', $url, $options, $body, $auth);
    }

    /**
     * @param string $url
     * @param array $options
     * @param Authenticatable|null $auth
     * @return RestngResponseInterface
     * @throws RestngException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $url, array $options = [], Authenticatable $auth = null): RestngResponseInterface
    {
        return $this->request('DELETE', $url, $options, [], $auth);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @param array $body
     * @param Authenticatable|null $auth
     * @return RestngResponseInterface
     * @throws RestngException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request(string $method, string $url, array $options = [], array $body = [], Authenticatable $auth = null): RestngResponseInterface
    {
        if ($auth !== null) {
            $options['token'] = $auth->getToken($this);
        }

        $response = $this->httpClient->request(
            $method,
            sprintf('%s/%s', $this->restngUrl, ltrim($url, '/')),
            $options,
            $body
        );

        $statusCode = $response->getStatusCode();
        $responseBody = $response->getResponseBody();

        if ($statusCode >= 400) {
            throw new RestngException(
                $method,
                $url,
                $responseBody['data'] ?? [],
                $statusCode
            );
        }

        return $this->buildResponse(
            $responseBody,
            $statusCode
        );
    }

    /**
     * @param array $responseBody
     * @param int $statusCode
     * @return RestngResponse
     */
    private function buildResponse(array $responseBody, int $statusCode): RestngResponseInterface
    {
        if (isset($responseBody['total'])
            && isset($responseBody['currentUrl'])
            && isset($responseBody['firstUrl'])
            && isset($responseBody['lastUrl'])) {
            return new RestngPageableResponse(
                $responseBody['data'],
                $statusCode,
                intval($responseBody['total']),
                $responseBody['currentUrl'],
                $responseBody['firstUrl'],
                $responseBody['lastUrl']
            );
        }

        return new RestngResponse(
            $responseBody['data'],
            $statusCode
        );
    }
}
