<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Responses;

/**
 * Class RestngPageableResponse
 * @package MiamiOH\LaravelRestng\Responses
 */
class RestngPageableResponse extends RestngResponse
{
    /**
     * @var int
     */
    private $total;
    /**
     * @var string
     */
    private $currentUrl;
    /**
     * @var string
     */
    private $firstUrl;
    /**
     * @var string
     */
    private $lastUrl;

    /**
     * RestngPageableResponse constructor.
     *
     * @param array $data
     * @param int $statusCode
     * @param int $total
     * @param string $currentUrl
     * @param string $firstUrl
     * @param string $lastUrl
     */
    public function __construct(array $data, int $statusCode, int $total, string $currentUrl, string $firstUrl, string $lastUrl)
    {
        parent::__construct($data, $statusCode);
        $this->total = $total;
        $this->currentUrl = $currentUrl;
        $this->firstUrl = $firstUrl;
        $this->lastUrl = $lastUrl;
    }

    /**
     * @return bool
     */
    public function isPageable(): bool
    {
        return true;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getCurrentUrl(): string
    {
        return $this->currentUrl;
    }

    /**
     * @return string
     */
    public function getFirstUrl(): string
    {
        return $this->firstUrl;
    }

    /**
     * @return string
     */
    public function getLastUrl(): string
    {
        return $this->lastUrl;
    }
}
