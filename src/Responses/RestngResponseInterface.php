<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Responses;

/**
 * Interface RestngResponseInterface
 * @package MiamiOH\LaravelRestng\Responses
 */
interface RestngResponseInterface
{
    /**
     * Get HTTP status code from RESTng
     *
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * Get data payload
     *
     * @return array
     */
    public function getData(): array;

    /**
     * @return bool
     */
    public function isPageable(): bool;
}
