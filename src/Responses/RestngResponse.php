<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Responses;

/**
 * Class RestngResponse
 * @package MiamiOH\LaravelRestng\Responses
 */
class RestngResponse implements RestngResponseInterface
{
    /**
     * @var array
     */
    protected $data;
    /**
     * @var int
     */
    protected $statusCode;

    /**
     * RestngResponse constructor.
     * @param array $data
     * @param int $statusCode
     */
    public function __construct(array $data, int $statusCode)
    {
        $this->data = $data;
        $this->statusCode = $statusCode;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return bool
     */
    public function isPageable(): bool
    {
        return false;
    }
}
