<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 10/9/18
 * Time: 1:59 PM
 */

namespace MiamiOH\LaravelRestng\Auth;

use MiamiOH\LaravelRestng\RestngClient;

interface Authenticatable
{
    public function getUsername(): string;
    public function getToken(RestngClient $restngClient): string;
    public function isValid(RestngClient $restngClient): bool;
}
