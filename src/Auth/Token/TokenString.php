<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Auth\Token;

use Carbon\Carbon;
use MiamiOH\LaravelRestng\Auth\Authenticatable;
use MiamiOH\LaravelRestng\RestngClient;

/**
 * Class TokenStringCredential
 * @package MiamiOH\LaravelRestng\Auth\Token
 */
class TokenString implements Authenticatable
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $token;

    /**
     * TokenStringCredential constructor.
     * @param string $username
     * @param string $token
     */
    public function __construct(string $username, string $token)
    {
        $this->username = $username;
        $this->token = $token;
    }

    /**
     * @param RestngClient $restngClient
     * @return string
     */
    public function getToken(RestngClient $restngClient): string
    {
        return $this->token;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function isValid(RestngClient $restngClient): bool
    {
        try {
            $res = $restngClient->get('/authentication/v1/'.$this->token);
            $body = $res->getData();

            $expired = Carbon::createFromFormat('Y-m-d\TH:i:s', $body['expiration_time']);

            return $expired->greaterThan(Carbon::now())
                && $body['username'] === $this->username;
        } catch (\Exception $e) {
            return false;
        }
    }
}
