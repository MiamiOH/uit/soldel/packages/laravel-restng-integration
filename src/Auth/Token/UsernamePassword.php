<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 10/9/18
 * Time: 2:01 PM
 */

namespace MiamiOH\LaravelRestng\Auth\Token;

use Carbon\Carbon;
use MiamiOH\LaravelRestng\Auth\Authenticatable;
use MiamiOH\LaravelRestng\RestngClient;

class UsernamePassword implements Authenticatable
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;
    /**
     * @var RestngToken
     */
    private $restngToken;

    /**
     * UsernamePasswordCredential constructor.
     *
     * @param string $username
     * @param string $password
     */
    public function __construct(
        string $username,
        string $password
    ) {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @param RestngClient $restngClient
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MiamiOH\LaravelRestng\Exceptions\RestngException
     */
    public function getToken(RestngClient $restngClient): string
    {
        if ($this->restngToken === null || $this->restngToken->isExpired()) {
            $response = $restngClient->post('/authentication/v1', [], [
                'username' => $this->username,
                'password' => $this->password,
                'type' => 'usernamePassword',
            ]);

            $data = $response->getData();

            $this->restngToken = new RestngToken(
                $data['token'],
                $data['username'],
                $data['credentialSource'],
                Carbon::createFromFormat(
                    'Y-m-d\TH:i:s',
                    $data['tokenLifetime']
                )
            );
        }

        return $this->restngToken->getToken();
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function isValid(RestngClient $restngClient): bool
    {
        try {
            $this->getToken($restngClient);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
