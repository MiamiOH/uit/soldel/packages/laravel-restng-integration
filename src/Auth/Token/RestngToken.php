<?php
/**
 * Created by PhpStorm.
 * User: liaom
 */

namespace MiamiOH\LaravelRestng\Auth\Token;

use Carbon\Carbon;

/**
 * Class RestngToken
 * @package MiamiOH\LaravelRestng\Auth\Token
 */
class RestngToken
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $source;
    /**
     * @var Carbon
     */
    private $expiredAt;

    /**
     * RestngToken constructor.
     * @param string $token
     * @param string $username
     * @param string $source
     * @param Carbon $expiredAt
     */
    public function __construct(string $token, string $username, string $source, Carbon $expiredAt)
    {
        $this->token = $token;
        $this->username = $username;
        $this->source = $source;
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return Carbon
     */
    public function getExpiredAt(): Carbon
    {
        return $this->expiredAt;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->getExpiredAt()->lessThanOrEqualTo(Carbon::now());
    }
}
