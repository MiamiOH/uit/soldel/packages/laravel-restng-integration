# Laravel RESTng Client For Laravel 5
RESTng resources are frequently used in the Laravel application. Instead of
setup/config Guzzle http client, handle authentication, write a lot of duplicated
code in the each application, this package provide a easy-to-use interface for accessing
RESTng resources in the Laravel application.
 
## Installation

Require this package with composer using the following command:
```bash
composer require miamioh/laravel-restng-integration
```

After updating composer, add the service provider to the `providers` array in 
`config/app.php`
```php
'providers' => [
    // Other Service Providers

    MiamiOH\LaravelRestng\Laravel\Providers\LaravelRestngServiceProvider::class,
],
```

> **Note:** 
> Laravel 5.5 uses Package Auto-Discovery, so it is not necessary for you 
> to manually add the service provider

Publish a configuration file by running the following `artisan` command:
```bash
php artisan vendor:publish --tag=restng
```

This will copy the configuration file to `config/restng.php`

Add RESTng base URL to your `.env` file:
```text
RESTNG_URL=https://wsdev.apps.miamioh.edu/api
```

If you plan to use any protected RESTng resource, you need to add `RESTNG_USERNAME`
and `RESTNG_PASSWORD` in the `.env` file:
```text
RESTNG_USERNAME=<put username here>
RESTNG_PASSWORD=<put password here>
```

#### Guzzle HTTP Client SSL Verification
this package uses Guzzle HTTP client to send RESTful request to
RESTng endpoint. By default, Guzzle HTTP client will not send request
to a web servier with self-signed certificate. If you plan to use
a RESTng resource hosted on your local machine/VM, you may get this error:
```text
cURL error 60: SSL certificate problem: self signed certificate (see http://curl.haxx.se/libcurl/c/libcurl-errors.html)
``` 
In order to bypassing certificate verification, you need to set `RESTNG_SSL_VERIFY` to `false` in the `.env` file:
```test
RESTNG_SSL_VERIFY=false
```
> **Note:** 
> DO NOT set `RESTNG_SSL_VERIFY` to `false` in the PRODUCTION environment.
> Disable ssl certificate verification is absolutely discouraged. 

## Examples

Example 1:  
To make a `GET` request to RESTng resource `/person/v3?pidm=123456`:
```php
$restngClient = resolve(RestngClient::class);
$response = $restngClient->get('/person/v3', ['pidm' => 1440394]);

$response->getStatusCode(); // 200
$response->getData(); // ['firstName' => '...', 'lastName' => '...', ...]
```

Example 2:  
To make a `POST` request to protected resource `/notification/v1/email/message` 
with token:
```php
$restngClient = resolve(RestngClient::class);
$credentialPool = resolve(CredentialPool::class);

$response = $restngClient->post(
    '/notification/v1/email/message', 
    [], // no query parameter
    [
        'toAddr' => '...',
        'fromAddr' => '...',
        'subject' => '...',
        'body' => '...'
    ], // request body
    $credentialPool->get() // by default, it retrieves default token credential. You can specify credential name to use different token credential
)
```

> **Tip:** use `$credentialPool->get()->getUsername()` to get username of a credential

Example 3:  
You may get 400 level or 500 level status code from RESTng because something is wrong:
```php
$restngClient = resolve(RestngClient::class);
try {
    $response = $restngClient->get('/person/v3', ['pidm' => 'abcdef']) // an invalid pidm
} catch(RestngException $e) {
    $e->getStatusCode(); // 500
    $e->getData(); // ['message' => 'Invalid pidm', ...]
}
```

## Usage
There are several way to use `RestngClient` in you code:  
 - via dependency injection (e.g. through constructor in the controller)
 - use `$client = resolve(RestngClient::class)` in any place
 
### Http Request

#### GET
function `get(...)`  
parameters:  
 - url: string, url of RESTng resource (required)
 - options: array, query parameters, e.g. `?attr1=3&attr2=abc` (default: `[]`)  
 - credential: currently we only support token credential (default: `null`)
 
#### POST
function `post(...)`  
parameters:  
 - url: string, url of RESTng resource (required)
 - options: array, query parameters, e.g. `?attr1=3&attr2=abc` (default: `[]`)  
 - body: array, request body (default: `[]`)
 - credential: currently we only support token credential (default: `null`)
 
#### PUT
function `put(...)`  
parameters:  
 - url: string, url of RESTng resource (required)
 - options: array, query parameters, e.g. `?attr1=3&attr2=abc` (default: `[]`) 
 - body: array, request body (default: `[]`) 
 - credential: currently we only support token credential (default: `null`)
 
#### DELETE
function `delete(...)`  
parameters:  
 - url: string, url of RESTng resource (required)
 - options: array, query parameters, e.g. `?attr1=3&attr2=abc` (default: `[]`)  
 - credential: currently we only support token credential (default: `null`)

### Response
The response return by `get()`, `post()`, `put()` or `delete()` is `RestngResponse`.
```php
// get status code
$response->getStatusCode(); // e.g. 200, 400, 404, etc.

// get payload/data
$response->getData(); // an array

// check if it's pageable
$response->isPageable(); // true or false
```

If a response is pageable, you will be able to access following additional methods:
```php
// get total number of items in the collection 
// (refer to $response->setTotalObjects($totalRecords) in the RESTng service)
$response->getTotal(); // an integer 

// get current page link
$response->getCurrentUrl();

// get first page link
$response->getFirstUrl();

// get last page link
$response->getLastUrl();
```

### Exceptions
If RESTng return 400 level or 500 level status code, you will get a `RestngException`
Exception. It offers following methods:
```php
try {
    // http request
} catch (RestngException $e) {
    // get status code
    $e->getStatusCode(); // e.g. 404, 400, etc.
    
    // get response body (payload)
    $e->getData(); // an array
}
``` 

### Credentials
Currently we only support token credential. Under current design, it is very easy to
add other credential type to this package in the future (after RESTng supporting it), 
e.g. oauth.

#### Token Credential
All token credentials defined in the `config/restng.php` file and retrieved from `.env`
file are stored in a credential pool during runtime. 
```php
$credentialPool = resolve(CredentialPool::class);

$credentialPool->get(); // retrieve default token credential
$credentialPool->get('my-token'); // retrieve token credential by name "my-token"
```

To add additional token credentials, you need to first define an entry in the 
`config/restng.php` file:
```php
[
    // Other configuration
    
    'credentials' => [
        'token' => [
            'default' => [
                'type' => 'usernamePassword',
                'username' => env('RESTNG_USERNAME', ''),
                'password' => env('RESTNG_PASSWORD', '')
            ],
            
            // you can add additional token credentials below, for example:
            //
            // add another username/password credential
            'my-token' => [
                'type' => 'usernamePassword',
                'username' => env('RESTNG_MY_TOKEN_USERNAME', ''),
                'password' => env('RESTNG_MY_TOKEN_PASSWORD', '')
            ],
            
            // or you may want to add a long-lived token
            'third-token' => [
                'type' => 'tokenString',
                'username' => env('RESTNG_USERNAME', ''),
                'token' => env('RESTNG_MY_LONG_LIVED_TOKEN', '')
            ]
        ]
    ]
]
```
Then you need to add corresponding secret in the `.env` file:
```test
RESTNG_MY_TOKEN_USERNAME=user2
RESTNG_MY_TOKEN_PASSWORD=password2

RESTNG_MY_LONG_LIVED_TOKEN=PT3yRRgDfHcNESfCWOeNz38N08wxgudFKd
```

> **Note:**  
> In order to saving the cost of getting a new token, token got
> from `CredentialPool->get()` will not be refreshed unless it
> expired.

## Unit Testing

### Use via dependency injection

Consider following code:
```php
class Example {
    private $restngClient;
    
    public function __construct(RestngClient $restngClient) {
        $this->>restngClient = $restngClient;
    }
    
    // ... some other code ..
    
    public function getPersonLastNameByPidm(string $pidm): string {
        $response = $this->restngClient->get('/person/v3', ['pidm' => $pidm]);
        $data = $response->getData;
        return $data[0]['lastName'] ?? '';
    }
}
```
And unit test:
```php
public function testGetPersonLastNameByPidm() {
    $restngClient = $this->createMock(RestngClient::class);
    $restngClient->method('get')
        ->willReturn(
            new RestngResponse(
                [['firstName' => 'first', 'lastName' => 'last', ...]],
                200
            )
        );

    $example = new Example($restngClient);
    $lastName = $example->getPersonLastNameByPidm('123456');
    
    $this->assertEquals('last', $lastName);
}
```

### Use via resolve() function
Consider following code:
```php
class Example2 {
    public function getPersonLastNameByPidm(string $pidm): string {
        $restngClient = resolve(RestngClient::class);
        $response = $restngClient->get('/person/v3', ['pidm' => $pidm]);
        $data = $response->getData;
        return $data[0]['lastName'] ?? '';
    }
}
```

And unit test:
```php
public function testGetPersonLastNameByPidm() {
    $restngClient = $this->createMock(RestngClient::class);
    $restngClient->method('get')
        ->willReturn(
            new RestngResponse(
                [['firstName' => 'first', 'lastName' => 'last', ...]],
                200
            )
        );
        
    $this->app->instance(RestngClient::class, $restngClient); // replace RestngClient instance in the laravel container with your mocked RestngClient

    $example2 = new Example2();
    $lastName = $example2->getPersonLastNameByPidm('123456');
    
    $this->assertEquals('last', $lastName);
}
```

## Use without Laravel
It is possible to use this package in the plain PHP project.

Example:
```php
$guzzleClient = new Client();

$restngClient = new RestngClient($guzzleClient, 'https://wsdev.apps.miamioh.edu/api');

$response = $restngClient->get(
    '/person/v3', // resource url
    ['pidm' => '12345'], // query parameter
    new UsernamePassword('username', 'password')
);

$response->getData(); // payload
$response->getStatusCode();
```
